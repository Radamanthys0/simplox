
module.exports = function (app) {

  app.get("/Default/Get", function (req, res) {
    try {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");

      res.status(200).json({ Content: "Conectado ao servidor de aplicação com sucesso!" });
    } catch (err) {
      console.log(err.message);
      res.status(500).json({ "error": err.message });
    }
  });

  app.post("/Defaul/Simplex", function (req, res) {
    try {
      // receber as coisa
      // console.log(req.query);

      var data = JSON.parse(req.query.object);
      //====================== < Executo Simplex > ============================
      //===============================< variaveis >==============================
      var tamanhoVariaveis = data.variaveis.length;
      var tamanhoRestricoes = data.restricoes.length;

      var tabela = [];
      var tabelaAux = [];

      var variaveisBasicas = [];
      var variaveisNaoBasicas = [];

      var resultado;
      var z;
      var saida = new Array(tamanhoVariaveis + tamanhoRestricoes + 1);

      // ===========================< Preparar entrada >==============================

      for (i = 0; i < tamanhoRestricoes + tamanhoVariaveis + 1; i++) {
        saida[i] = 0;
      }

      for (i = 0; i < tamanhoRestricoes + 1; i++) {
        linhaAux = [];
        for (j = 0; j < tamanhoVariaveis + 1; j++) {
          linhaAux[j] = -1;
        }
        tabelaAux[i] = linhaAux;
      }
      ///console.log("inicia MatrizAux")
      //console.log(tabelaAux);

      /**
       * coloca FO na matriz, para isso é colocado na forma padrao
       */
      //function startFO(){

      var x = data.max ? 1 : -1;

      var linha = [];
      linha.push(0)// = 0;
      for (i = 1; i < tamanhoVariaveis + 1; i++) {
        linha.push(data.variaveis[i - 1].value * x);
      }
      tabela.push(linha)//[0] = linha;
      //}


      /**
       * coloca as Restricoes na matriz, para isso é colocado na forma padrao
       */
      //function startRestr(){

      for (i = 1; i < tamanhoRestricoes + 1; i++) {
        var blaLinha = [];
        var y = data.restricoes[i - 1].sinal.alias == "<=" ? 1 : -1;
        blaLinha[0] = data.restricoes[i - 1].termo * y;
        for (j = 1; j < tamanhoVariaveis + 1; j++) {
          blaLinha[j] = data.restricoes[i - 1].variaveis[j - 1].value * y;
        }

        tabela[i] = blaLinha;
      }
      //}
      /**
       * inicializa os vetores de variveis basicas e nao basicas
       * Repare que cada posição vai ter um indice q corresponde a x1, x2, x3...
       * a pos 0 de cada vetor diz respeito a:
       * ML no variaveisNAOBasicas
       * f(x) no var iaveisBasicas
       */
      //function startVariaveisBasicas(){

      for (i = 0; i < tamanhoVariaveis + 1; i++) { variaveisNaoBasicas[i] = i }
      variaveisBasicas[0] = 0;
      for (i = 1; i < tamanhoRestricoes + 1; i++) { variaveisBasicas[i] = i + tamanhoVariaveis }

      // ==============================< SIMPLEX >===============================

      //=================< fucoes de verificacao >======================
      /**
       * Função que verifica se é possivel
       * 
       */
      function isPossivel() {
        for (j = 1; j < tamanhoVariaveis + 1; j++) {
          if (tabela[0][j] >= 0.0) {
            return false;
          }
        }
        return true;
      }


      /**
       * Metodo que descobre se o simplex é indeterminado
       */
      function isIndeterminado() {
        var hasZero = false;
        for (j = 1; j < tamanhoVariaveis + 1; j++) {
          if (tabela[0][j] >= 0) {
            if (tabela[0][j] == 0) {
              hasZero = true;
            } else {
              return false
            }
          }
        }
        if (hasZero) {
          return true;
        } else {
          return false;
        }

      }

      /**
       * Metodo que descobre se o simplex é ilimitado
       */
      function isIlimitado() {
        var hasPos = false;
        for (j = 1; j < tamanhoVariaveis + 1; j++) {
          if (tabela[0][j] >= 0) {
            hasPos = true;
            for (i = 1; i < tamanhoRestricoes + 1; i++) {
              if (tabela[i][j] >= 0) {
                return false;
              }
            }
          }
        } if (hasPos) {
          return true;
        } else {
          return false;
        }
      }

      /**
       * Metodo que descobre se existe numeros negativos na coluna do ML
       */
      function hasNegML() {
        for (i = 1; i < tamanhoRestricoes + 1; i++) {
          if (tabela[i][0] < 0) {
            return true;
          }
        }
        return false;
      }

      /**
       * Metodo que descobre se o simplex nao tem solucao
       */
      function isImpossivel() {
        var hasNeg = false;
        for (i = 1; i < tamanhoRestricoes + 1; i++) {
          if (tabela[i][0] < 0) {
            hasNeg = true;
            for (j = 1; j < tamanhoVariaveis + 1; j++) {
              if (tabela[i][j] < 0) {
                return false
              }
            }
          }
        }
        if (hasNeg) {
          return true;
        }
        else {
          return false;
        }
      }

    //=================< SIMPLEX propiramente dito >======================

      /**
       * Esta função representa a fase 1
       */
      function faseUm() {
        var colunaPermitida = getColunaPermitidaF1();
        var linhaPermitida = getLinhaPermitida(colunaPermitida);
        troca(colunaPermitida, linhaPermitida);
     
      }

      /**
       * Algoritmo que executa a fase 2
       */
      function faseDois() {
        var colunaPermitida = getColunaPermitidaF2();
        var linhaPermitida = getLinhaPermitida(colunaPermitida);
        //console.log("linhaP " + linhaPermitida + " colunaP " + colunaPermitida);
        troca(colunaPermitida, linhaPermitida);
      }

      /**
       * Algoritmo de bisca para a coluna permitida na fase 1
       */
      function getColunaPermitidaF1() {
        for (i = 1; i < tamanhoRestricoes + 1; i++) {
          if (tabela[i][0] < 0) {
            for (j = 1; j < tamanhoVariaveis + 1; j++) {
              if (tabela[i][j] < 0) {
                return j;
              }
            }
          }
        }
      }

      /**
       * Algoritmo de bisca para a coluna permitida na fase 2
       */
      function getColunaPermitidaF2() {
        for (i = 1; i < tamanhoVariaveis + 1; i++) {
          if (tabela[0][i] > 0) {
            return i;
          }
        }
      }

      /**
       * algoritmo de busca da linha permitida
       * @param {*valor da coluna permitida} colPermida 
       */
      function getLinhaPermitida(colPermida) {
        var teste = 9999999;
        var linhaPermitida;
        for (i = 1; i < tamanhoRestricoes + 1; i++) {
          if (((tabela[i][colPermida] > 0 && tabela[i][0] > 0) || (tabela[i][colPermida] < 0 && tabela[i][0] < 0)) && (tabela[i][colPermida] != 0)) {
            if ((tabela[i][0] / tabela[i][colPermida]) < teste) {
              teste = tabela[i][0] / tabela[i][colPermida];
              linhaPermitida = i;
            }
          }
        }
        return linhaPermitida;
      }

      /**
       * Algoritmo de troca
       * @param {*valor da coluna Permitida} colPermitda 
       * @param {*valoer da linha Permitida} linPermitida 
       */
      function troca(colPermitda, linPermitida) {
        var pivoInverso = 1 / (tabela[linPermitida][colPermitda]);
        tabelaAux[linPermitida][colPermitda] = pivoInverso;

        // calculo a parte de baixo da linha
        for (i = 0; i < tamanhoVariaveis + 1; i++) {
          if (i != colPermitda) {
            tabelaAux[linPermitida][i] = tabela[linPermitida][i] * pivoInverso;
          }
        }
        // calculo a parte de baixo da coluna
        for (j = 0; j < tamanhoRestricoes + 1; j++) {
          if (j != linPermitida) {
            tabelaAux[j][colPermitda] = tabela[j][colPermitda] * pivoInverso * -1;
          }
        }
        // calculo o restante da tabela
        for (i = 0; i < tamanhoRestricoes + 1; i++) {
          for (j = 0; j < tamanhoVariaveis + 1; j++) {
            if (i != linPermitida && j != colPermitda) {
              tabelaAux[i][j] = tabelaAux[i][colPermitda] * tabela[linPermitida][j]
            }
          }
        }
        // atualzacao dos vetores que represetam as variaveis basicas e nao basicas
        var aux = variaveisNaoBasicas[colPermitda];
        variaveisNaoBasicas[colPermitda] = variaveisBasicas[linPermitida];
        variaveisBasicas[linPermitida] = aux;

        for (i = 0; i < tamanhoRestricoes + 1; i++) {
          for (j = 0; j < tamanhoVariaveis + 1; j++) {
            if (i == linPermitida || j == colPermitda) {
              tabela[i][j] = tabelaAux[i][j];
            }
            else {
              tabela[i][j] = tabela[i][j] + tabelaAux[i][j];
            }
          }
        }
      }

      /**
       * Simplex propriamente dito
       */
      function Simplex() {
        var termina = false;
        while (!termina) {
          if (isImpossivel()) {
            resultado = "Impossivel";
            termina = true;
          } else if (hasNegML()) {
            faseUm();
          } else {
            if (isPossivel()) {
              resultado = "possivel";
              termina = true;
            } else if (isIndeterminado()) {
              resultado = "Indeterminado";
              termina = true;
            } else if (isIlimitado()) {
              resultado = "Ilimitado";
              termina = true;
            } else {
              faseDois();
            }
          }
        }
      }

      function getResposta() {
        // pega o |z| e coloca no vetor de resposta
        if (tabela[0][0] < 0) {
          saida[0] = tabela[0][0] * -1;
        } else {
          saida[0] = tabela[0][0];
        }// coloca as variaveis no vetor de saida
        for (j = 1; j < tamanhoRestricoes + 1; j++) {
          saida[variaveisBasicas[j]] = tabela[j][0];
        }
      }


      Simplex();
      getResposta();

      // ==================< envio JSON >==============================
      res.status(200).json({ "saida": saida, "z": saida[0], "resultado":resultado });

    } catch (e) {
      res.status(500).json({ "error": e.message });
    }
  });


}
