var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function (req, res) {
    res.redirect("/index.html");
});

router.get('/', function(req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,contenttype');
    res.setHeader('Access-Control-Allow-Credentials', true);

    res.send('cors problem fixed:)');
});

module.exports = router;
