
app = angular.module('SimplexApp', []);

app.controller('SimplexController', SimplexController);

connectionUtilities.sendAjaxRequest(
  "http://localhost:3003/Default/Get", "GET", undefined,
  function(data){
      if(data){
        domUtilities.showToast(data.Content, 3000, "success");
      }
  }.bind(this),
  function(jqXHR, textStatus, errorThrown){
    domUtilities.showToast("Não foi possível conectar ao servidor de aplicação", 3000, "error");
  }.bind(this), true,
  function(jqXHR, textStatus){
    setTimeout(function(){
      domUtilities.toggleView(".progress");
    }, 3000);

  }.bind(this),
  function(){
    domUtilities.toggleView(".progress");
  }
);

$(document).ready(function(){
  // the "href" attribute of the modal trigger must specify the modal ID that wants to be triggered
  $('#funcObj').modal({
    dismissible: false
  });
  $('select').material_select();
});
