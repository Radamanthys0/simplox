function DomUtilities() {

    this.showToast = function (mensagem, tempo, typeMessage, callback) {
        var classe;
        switch (typeMessage) {
            case 'error':
                classe = 'red darken-1';
                break;
            case 'success':
                classe = 'green darken-1';
                break;
            case 'info':
                classe = 'blue lighten-2';
                break;
            case 'warning':
                classe = 'orange lighten-2';
            default:
                classe = 'grey darken-1';

        }

        Materialize.toast(mensagem, tempo, classe, callback);
    }

    this.addCssClasses = function (element, classesArray) {
        if (!element || !classesArray || !(classesArray instanceof Array))
            return;

        for (let classIndex = 0; classIndex < classesArray.length; classIndex++) {
            $(element).addClass(classesArray[classIndex]);
        }
    };

    this.removeCssClasses = function (element, classesArray) {
        if (!element || !classesArray || !(classesArray instanceof Array))
            return element;

        for (let classIndex = 0; classIndex < classesArray.length; classIndex++) {
            $(element).removeClass(classesArray[classIndex]);
        }

        return element;
    };

    this.getWidth = function(element){
      if(!element) throw "Element not found";

      return $(element).width();
    };

    this.getHeigth = function(element){
      if(!element) throw "Element not found";

      return $(element).heigth();
    };

    this.hasClass = function (element, classToFind) {
      if (!element || !classToFind)
          return;

      return $(element).hasClass(classToFind);
    };

    this.toggleView = function(element){
      if(!element)
        throw "Element not found";

      return $(element).toggle();
    };
}

var domUtilities = new DomUtilities();
