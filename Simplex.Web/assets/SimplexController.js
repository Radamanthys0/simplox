function SimplexController($scope) {
  this.simplexObj = {
    max: false,
    variaveis: [
      { value: 0 },
      { value: 0 }
    ],
    restricoes: [
      { variaveis: [{ value: 0 }], sinal: ">=", termo: 0 }
    ]
  };
  this.sinais = [
    { value: "greater-equal", alias: ">=", numericValue: 1 },
    { value: "less-equal", alias: "<=", numericValue: 2 }
  ];
  this.simplexResponse = undefined;

  this.test = function () {
    debugger;
  }.bind(this);

  this.addVariavel = function () {
    this.simplexObj.variaveis.push({ value: 0 });
    if (!$scope.$$phase) $scope.$apply();
  };

  this.addRest = function () {
    this.simplexObj.restricoes.push({ variaveis: [{ value: 0 }], sinal: "", termo: 0 });
    if (!$scope.$$phase) $scope.$apply();
  }.bind(this);

  this.addField = function (obj) {
    obj.variaveis.push({ value: 0 });
    if (!$scope.$$phase) $scope.$apply();
  };

  this.removeVal = function (rest, obj) {
    var valor = obj;
    var index = rest.variaveis.findIndex(function (e) {
      return e == valor;
    });
    rest.variaveis.splice(index, 1);
    if (!$scope.$$phase) $scope.$apply();
  }.bind(this);

  this.removeOpt = function (obj) {
    var valor = obj;
    var index = this.simplexObj.variaveis.findIndex(function (e) {
      return e == obj;
    });
    this.simplexObj.variaveis.splice(index, 1);
    if (!$scope.$$phase) $scope.$apply();
  }.bind(this);

  this.removeRest = function (obj) {
    var valor = obj;
    var index = this.simplexObj.restricoes.findIndex(function (e) {
      return e == obj;
    });
    this.simplexObj.restricoes.splice(index, 1);
    if (!$scope.$$phase) $scope.$apply();
  }.bind(this);

  this.send = function () {
    $(".progress").toggle();
    setTimeout(function () {

      delete this.simplexObj.$$hashKey;
      this.simplexObj.variaveis.map(function (e) {
        delete e.$$hashKey;
      });
      this.simplexObj.restricoes.map(function (e) {
        delete e.sinal.$$hashKey;
        e.variaveis.map(function (f) {
          delete f.$$hashKey;
        });
        delete e.$$hashKey;
      });

      var data = this.simplexObj;

      var url = "http://localhost:3003/Defaul/Simplex?object=" + JSON.stringify(data);
      connectionUtilities.sendAjaxRequest(url, "POST", undefined,
        function (data) {
          // trato o q veio do webServer
          this.simplexResponse = data;
          if(!$scope.$$phase) $scope.$apply()

        }.bind(this),
        function (jqXHR, erroThrown, statusText) {
        }, false,
        function () {
          domUtilities.toggleView(".progress");
        }, function () {
          domUtilities.toggleView(".progress");
        });

    }.bind(this), 0);
  }.bind(this);
};
