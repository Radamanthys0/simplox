var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var ejs = require('ejs');
var path = require('path');
var fs = require("fs");


var app = express();
app.use(bodyParser.json()); // for parsing application/json

var httpPort = process.env.npm_package_config_httpPort;
app.set('httpPort', process.env.HTTP_PORT || httpPort || 3576);

var httpServer = http.createServer(app);
httpServer.listen(app.get('httpPort'));
console.log("App started at http://localhost:" + app.get('httpPort'));

app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, 'Simplex.Web')));


var index = require('./routes/routes');
require('./routes/platform')(app);
app.use('/', index);

//add headers
app.use(function (req, res, next) {
	console.log(req);
	console.log(res);
	console.log(next);
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

module.exports = app;
